# Assessment: Templating Challenge
Stelle dir vor, du kriegst folgendes Layout zur Umsetzung als HTML-Template damit es es ein Backend-Developer in eine Web-Applikation integrieren kannn. 

![template_preview.png](readme/template_preview.png)
*Screenshot:* `/assessment-files/Template.sketch`

## Deine Aufgaben ##
* Erstelle  ein Template (HTML / CSS / JS) welches möglichst nahe an das Layout (/assessment-files/Template.sketch) kommt. 
* Beschreibe deine Lösung kurz und erkläre welche Überlegungen du dir gemacht hast.
* Triff bei Unklarheiten Annahmen und begründe diese. 
* Verwende (falls irgendwie möglich) das Bootstrap-Framework. 

## Tipps ##
* Bilder, Farbcodes, Font-Sizes etc. findest du im Sketch-File.
* Das Sketch-File lässt sich auch mit Adobe XD öffnen. 
* Pushe von Vorteil einzelne Arbeitsschritte mit [aussagenden Commit-Messages](https://sparkbox.com/foundry/semantic_commit_messages) ins Repository.  


## Hilfsmittel ##
Du darfst grundsätzlich alles verwenden, was in deinem normalen Arbeitsalltag auch zur Verfügung steht. 
